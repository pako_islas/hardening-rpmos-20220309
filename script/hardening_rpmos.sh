#!/bin/bash -x
#      HARDENING RPM-Based OS SCRIPT
#
##  THIS SCRIPT IS NOT IDEMPOTENT (YOU CANNOT RUN IT TWICE)
## CONFIRM YOU HAVE A SNAPSHOT BEFORE EXECUTE IT.
#
#  This script requires that /home has its own partition,
# the existance of a "rl" volume group with 4GiB of
# available space.
#
##  THIS SCRIPT IS NOT IDEMPOTENT (YOU CANNOT RUN IT TWICE)
## CONFIRM YOU HAVE A SNAPSHOT BEFORE EXECUTE IT.

# Validate there is enough space in VGS

configure_filesystem () {

    #  This section implements hardening for the filesystem by
    # creating the own mount points for `/var/log`, `/var/log/audit`,
    # configures `/home` mounting options, changes `/tmp` mount point,
    # add `shm` mount options.
    echo "Changing filesystem"

    # Backup dirs
    mkdir -p /hrdn/var_log_bkp
    mkdir -p /hrdn/var_log_audit_bkp
    mkdir -p /hrdn/tmp_bkp

    # Modify fstab
    cp /etc/fstab /etc/fstab.bkp
    sed -i '/[/]home/ s/^/#/' /etc/fstab
    cat fstab_configs >> /etc/fstab
    # update names fstab
    sed -i "s/CHANGEME/$selected_vg/g" /etc/fstab

    # Create PV and format them
    lvcreate -L 1.9G -n varloglv $selected_vg
    lvcreate -L 1.9G -n varlogauditlv $selected_vg
    mkfs.xfs -f /dev/$selected_vg/varloglv
    mkfs.xfs -f /dev/$selected_vg/varlogauditlv

    # Backup files
    mv /var/log/audit/* /hrdn/var_log_audit_bkp/
    mv /var/log/* /hrdn/var_log_bkp/

    # Mount LVs
    # Add "-v" to restorecon if it is necessary to check SELinux relabel process
    mount /var/log
    cp -R /hrdn/var_log_bkp/* /var/log/
    mount /var/log/audit
    cp -R /hrdn/var_log_audit_bkp/* /var/log/audit/
    restorecon -R /var/log

    # Tmp mounting procedure
    mv /tmp/* /hrdn/tmp_bkp/
    mount /var/tmp
    cp -R /hrdn/tmp_bkp/* /tmp/
    restorecon -R /tmp

    echo "Completed Disks Hardening"
}

validate_sticky_bit () {
    ## Check sticky bit is enabled for all world-writable directories
    ## This section should return empty
    df --local -P | awk {'if (NR!=1) print $6'} | xargs -I '{}' find '{}' -xdev -type d \( -perm -0002 -a ! -perm -1000 \) 2>/dev/null
}

enable_gpg_repositories () {
    ## GPG Enable Repositories
    # rpm -q gpg-pubkey --qf '%{name}-%{version}-%{release} --> %{summary}\n'
    cp /etc/yum.conf /etc/yum.conf.bkp
    sed -i 's/gpgcheck=[0-9]/gpgcheck=1/g' /etc/yum.conf
}

configure_packages () {
    ## OS packages configurations
    insecure_software="omi telnet rsh rlogin ypserv ypbind tftp talk chargen daytime echo tcpmux"
    yum -y remove $insecure_software
    if [[ $os_release == *"Maipo"* || $os_release == *"Core"* ]]; then
        required_sofware="vi"
    else
        required_sofware="chrony oddjob"
    fi
    yum -y install $required_sofware
    yum check-update >> /root/yum_check-update_$(date +'%Y%m%d_%H-%M-%S').log
    yum -y update
}

password_grub () {
    ## Protecting Grub
    echo -e "\n Insert Grub password: \n"
    grub2-setpassword
    chown root:root /boot/grub2/grub.cfg
    chmod og-rwx /boot/grub2/grub.cfg
}

configure_systemd_services () {

    ## Services
    unconfigured_services=( "kdump" "vsftpd" "bind" "dhcpd" "smb" "nmb" "httpd" "cockpit" )
    for unconfigured_service in "${unconfigured_services[@]}"
    do
        systemctl disable --now "$unconfigured_service"
    done
    required_services=( "sssd" "oddjobd" )
    for required_service in "${required_services[@]}"
    do
        systemctl enable --now "$required_service"
    done
}


validate_vaspace_umask () {
    ## Kernel variable should return 2
    vaspace=$(sysctl kernel.randomize_va_space | awk '{print $3}')
    if [ $vaspace -ne 2 ]; then
        echo "Error with sysctl kernel.randomize_va_space it should return 2"
        exit 1
    fi
    ## Umask Value
    #umask_value="$(grep "umask [0-9][0-9][0-9]" /etc/init.d/functions)"
    umask_value="$(umask)"
    if [ "$umask_value" == "0022" ] || [ "$umask_value" == "0027" ]; then
        echo umask OK
    else
        echo Critical error, recover snapshot, and set umask 022 before execute script
        exit 1
    fi
}

set_sysctl_variables () {
    cat sysctl_conf >> /etc/sysctl.d/99-sysctl.conf
    sysctl -p
    echo "set sysctl variables"
}

create_ssh_user () {
    ## Create user to connect via ssh
    useradd localroot
    usermod localroot -aG wheel
    echo "Insert password for new administrative user localroot"
    passwd localroot
}

configure_sshd () {
    ## Setup SSHD
    cp /etc/ssh/sshd_config /etc/ssh/sshd_config.bkp
    sed -i '/PermitRootLogin/ s/^/#/' /etc/ssh/sshd_config
    cat ssh_conf >> /etc/ssh/sshd_config
    systemctl restart sshd
    echo "configure sshd"
}

set_selinux_enforced () {
    ## Force SELinux to Enforced
    cp /etc/selinux/config /etc/selinux/config.bkp
    sed -i s/^SELINUX=.*$/SELINUX=enforcing/ /etc/selinux/config
}

configure_ntp () {
    ## Configure NTP
    timedatectl set-timezone Pacific/Auckland
    mv /etc/localtime /etc/localtime.back
    ln -s /usr/share/zoneinfo/Pacific/Auckland /etc/localtime
    systemctl enable --now chronyd
    timedatectl set-ntp true
}

configure_pam_rhel8 () {
    ## Pam https://access.redhat.com/solutions/5027331
    ## Passwd algorithm 512 by default
    grep 512 /etc/libuser.conf /etc/login.defs
    path_system_auth="/etc/authselect/custom/password-policy/system-auth"
    path_password_auth="/etc/authselect/custom/password-policy/password-auth"
    authselect apply-changes -b --backup=sssd.backup
    authselect create-profile password-policy -b sssd
    authselect select custom/password-policy --force
    authselect current
    authselect enable-feature with-mkhomedir
    authselect enable-feature with-faillock
    cp $path_system_auth /etc/authselect/custom/password-policy/system-auth.bkp
    cp $path_password_auth /etc/authselect/custom/password-policy/password-auth.bkp
    sed -i '/pam_pwquality.so/a password requisite pam_pwhistory.so remember=5 use_authtok' $path_system_auth
    sed -i '/pam_pwquality.so/a password requisite pam_pwhistory.so remember=5 use_authtok' $path_password_auth
    sed -i '/pam_pwquality.so/ s/$/ enforce_for_root/' $path_system_auth
    sed -i '/pam_pwquality.so/ s/$/ enforce_for_root/' $path_password_auth
    cat pwquality_conf >> /etc/security/pwquality.conf
    ## authselect apply-changes
    echo "pam_configuration"
}

secure_tty_pam_rhel8 () {
    ### Secure tty https://access.redhat.com/solutions/6758921
    path_pam_login="/etc/pam.d/login"
    path_pam_remote="/etc/pam.d/remote"
    cp $path_pam_login /etc/pam.d/login.bkp
    cp $path_pam_remote /etc/pam.d/remote.bkp
    sed -i '/PAM-1.0/a auth [user_unknown=ignore success=ok ignore=ignore default=bad] pam_securetty.so' $path_pam_login
    sed -i '/PAM-1.0/a auth required pam_securetty.so' $path_pam_remote
    cat securetty >> /etc/securetty
    chown root:root /etc/securetty
    chmod 400 /etc/securetty
}

configure_pam_other () {
    cp /etc/pam.d/other /etc/pam.d/other.bkp
    cat pam_other > /etc/pam.d/other
}

configure_pam_rhel7 () {
    cp /etc/pam.d/system-auth /etc/pam.d/system-auth.bkp
    sed -i '/use_authtok/ s/$/ remember=10/' /etc/pam.d/system-auth
}

configure_firewall () {
    ## Firewall
    firewall-cmd --zone=public --add-port=1270/tcp --permanent
    firewall-cmd --reload
}

configure_pwquality_rhel7 () {
    cp /etc/security/pwquality.conf /etc/security/pwquality.conf.bkp
    cat pwquality >> /etc/security/pwquality.conf
}

update_algorithm_rhel7 () {
    authconfig --passalgo=sha512 --update
}

securetty_rhel7 () {
    cp /etc/securetty /etc/securetty.bkp
    cat securetty > /etc/securetty
}

validate_execute_script () {
    # Ask user if the Machine has enough disk space
    case $yn in
    	yes )
            if test -f "$os_file"; then
    	        echo "ok, we will proceed"
            else
                echo "The operating system is not supported"
                echo "exiting..."
                exit 1
            fi
    	    ;;
    	no ) echo exiting...;
    		exit;;
    	* ) echo invalid response. Just yes or no;
    		exit 1;;
    esac
}

## main
echo -e "\nTHIS SCRIPT IS NOT IDEMPOTENT (YOU CANNOT RUN IT TWICE)\n"
echo -e "CONFIRM YOU HAVE A SNAPSHOT BEFORE EXECUTE IT\n."
read -p "Is there any VG that has at least 3.99 GiB of available space? (yes/no) " yn

## Variables
os_file=/etc/os-release
os_release=$(grep -ie "^Version=" /etc/os-release | cut -d "=" -f 2)

## Validate space and request VG
validate_execute_script
vgs
read -p "Which vg should use to configure the server?: " selected_vg

## Configurations complatible with RHEL 7 and RHEL 8
configure_filesystem
validate_sticky_bit
enable_gpg_repositories
configure_packages
password_grub
configure_systemd_services
validate_vaspace_umask
set_sysctl_variables
configure_pam_other
create_ssh_user
configure_sshd
set_selinux_enforced
configure_ntp
configure_firewall

case $os_release in
    *"Maipo"*)
        echo "RHEL 7"
        configure_pwquality_rhel7
        securetty_rhel7
        configure_pam_rhel7
    ;;
    *"Core"*)
        echo "CentOS 7"
        configure_pwquality_rhel7
        configure_pam_rhel7
        securetty_rhel7
    ;;
    *"Green Obsidian"*)
        echo "Rocky 8"
        configure_pam_rhel8
        secure_tty_pam_rhel8
    ;;
    *"Ootpa"*)
        echo "RHEL 8"
        configure_pam_rhel8
        secure_tty_pam_rhel8
    ;;
    * )
        echo "Server not supported"
        echo "Please rollback the OS to the last snapshot"
    ;;
esac

echo "Hardening complete"
